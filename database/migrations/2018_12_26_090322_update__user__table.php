<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->tinyInteger('plan_type')->defualt('0');
            $table->string('expire_date')->nullable();
            $table->tinyInteger('plan_status')->defualt('0');
            $table->tinyInteger('is_free')->defualt('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('plan_type');
            $table->dropColumn('expire_date');
            $table->dropColumn('plan_status');
            $table->dropColumn('is_free');
        });
    }
}
