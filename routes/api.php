<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Api\LoginController@register');
Route::post('login', 'Api\LoginController@login');
Route::post('social_login', 'Api\LoginController@social_login');
Route::post('forgot_password', 'Api\LoginController@forgot_password');
Route::get('log', 'Api\LoginController@log');

Route::group(['middleware' => 'jwt.verify'], function () {
	 Route::get('logout', 'Api\LoginController@logout');
	 Route::post('edit_profile', 'Api\LoginController@edit_profile');
	 Route::post('file-upload', 'Api\FileController@uploadFile');
	 Route::get('get-last-file', 'Api\FileController@getLastUploadedFile');	
	 Route::post('file-delete', 'Api\FileController@deleteFile');
	 Route::post('subscription', 'Api\LoginController@subscription');
});
