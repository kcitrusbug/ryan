<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use File;

class FileController extends Controller
{
    //
    public function uploadFile(Request $request)
    {
       $rules = array(
           
            'file' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => 400],400);
        }

        $data = [];
        $data['user_id'] = JWTAuth::touser($request->token)->id;
        // upload file start
        $filename = uniqid(time()) . '.' . $request->file->getClientOriginalExtension();
        if (!is_dir('uploads/user_files/'.$data['user_id'].'/')) {
            mkdir('uploads/user_files/'.$data['user_id'].'/', 0777, true);
        }
        $request->file->move('uploads/user_files/'.$data['user_id'].'/', $filename);
        // upload file end
        $data['file'] = $filename;
        
        $userFile = \App\UserFiles::create($data);
        $userFile->file = url('/').'/uploads/user_files/'.$data['user_id'].'/'.$filename;

        return response()->json([
            'data' => $userFile,
            'success' => true,
            'message' => 'File uploaded successfully.',
            'status'  => 200
        ], 200);
    }

    public function getLastUploadedFile(Request $request)
    {
        $user=JWTAuth::touser($request->token)->id;
        if(!$user){
            return response()->json([
                'data' => (Object)[],
                'success' => false,
                'message' => 'User id not found.',
                'status'  => 400
            ], 400);
        }

        $userFile = \App\UserFiles::where('user_id',$user)->orderBy('id','DESC')->first();
        
        if(!$userFile){
            return response()->json([
                'data' => (Object)[],
                'success' => false,
                'message' => 'File not exist with this user.',
                'status'  => 400
            ], 400);
        }
        $userFile->file = url('/').'/uploads/user_files/'.$user.'/'.$userFile->file;

        return response()->json([
            'data' => $userFile,
            'success' => true,
            'message' => 'User File.',
            'status'  => 200
        ], 200);
    }

    public function deleteFile(Request $request){

        $user=JWTAuth::touser($request->token)->id;
        if(!$user){
            return response()->json([
                'data' => (Object)[],
                'success' => false,
                'message' => 'User id not found.',
                'status'  => 400
            ], 400);
        }
        // get all the files of the User and delete it
        $userFiles = \App\UserFiles::where('user_id',$user)->get();
       
        foreach($userFiles as $user_file){
            // find the file in the folder if exist then delete it 
            if($user_file){
                if($user_file->file != null){
                    $file = public_path().'/uploads/user_files/'.$user.'/'.$user_file->file;
                    if (File::exists($file)) {
                        unlink($file);
                    }
                }
                $user_file->delete();
            }

        }
        return response()->json([
            'data' => (Object)[],
            'success' => true,
            'message' => 'User files deleted Successfully.',
            'status'  => 200
        ], 200);

    }

}
