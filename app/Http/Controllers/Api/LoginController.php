<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Password;
use Carbon\Carbon;

class LoginController extends Controller
{
    //
    public function register(Request $request)
    {
        if(isset($request->email) && isset($request->password) && isset($request->name))
        {
            $rules = array(
                    'email' => 'required|email|unique:users',
                    'password'=>'required',
                    'name' => 'required',
                );

            $validator = \Validator::make($request->all(), $rules, []);

            if ($validator->fails()) {
                    $validation = $validator;
                    $status = false;
                    $code = 400;
                    $msgArr = $validator->messages()->toArray();
                    $messages = reset($msgArr)[0];

                    return response()->json([
                                'message' =>$messages,
                                'success' => false,
                                'status' => $code]);
                }


            
            $user=new User();
            $user->name=$request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            return response()->json([
                
                'message' => 'Register Success.',
                'success' => true,
                'status' => 200,
            ]);

        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Parameter.',
                'status'  => 400
            ], 400);
        }
    }
    
	
	public function forgot_password(Request $request)
    {
        if(isset($request->email))
        {
            $user=User::where('email',$request->email)->first();
            if($user)
            {
                $credentials = ['email' =>  $request->email];
                $response = Password::sendResetLink($credentials, function (Message $message) {
                    $message->subject($this->getEmailSubject());
                });

                switch ($response) {
                    case Password::RESET_LINK_SENT:
                        return response()->json([
                            'success' => true,
                            'message' => trans($response),
                            'status'  => 200
                        ], 200);
                    case Password::INVALID_USER:
                        return response()->json([
                            'success' => false,
                            'message' => trans($response),
                            'status'  => 400
                        ], 400);
                }
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Email Id.',
                    'status'  => 400
                ], 400);
            }
        }
        else{
            return response()->json([
                'success' => false,
                'message' => 'Invalid Parameter.',
                'status'  => 400
            ], 400);
        }
    }
	
	
	public function login(Request $request)
    {
        if(isset($request->email) && isset($request->password))
        {
           $input = $request->only('email','password');

           $jwt_token = null;
           $user = User::where("email", $request->email)->first();

           if($user)
           {
                   if (Hash::check($request->password, $user->password))
                   {
                       if ($jwt_token = JWTAuth::attempt($input,['exp' =>Carbon::now()->addDays(7)->timestamp]))
                       {
                        $user=JWTAuth::user();
                        $user['token']=$jwt_token;

                        return response()->json([
                          'result' => $user,
                          'message' => 'Login Success.',
                          'success' => true,      
                          'status' => 200,
                        ]);
                       }
                       else
                       {
                           return response()->json([
                               'success' => false,
                               'message' => 'Your Account Has Not Activated Yet.',
                               'status'  => 401
                           ], 401);
                       }
                   }
                   else
                   {
                       return response()->json([
                           'success' => false,
                           'message' => 'Invalid Email or Password',
                           'status'  => 401
                       ], 401);
                   }
           }
           else
           {
               return response()->json([
                   'success' => false,
                   'message' => 'Invalid Email or Password',
                   'status'  => 401
               ], 401);
           }
        }
       else
        {
           return response()->json([
               'success' => false,
               'message' => 'Invalid Parameter.',
               'status'  => 400
           ], 400);
        }

    }
	
	
	public function logout(Request $request)
    {
        if(isset($request->token))
        {

            try {
                JWTAuth::invalidate($request->token);

                return response()->json([
                    'success' => true,
                    'message' => 'User logged out successfully',
                    'status'  => 200
                ], 200);

            } catch (JWTException $exception) {

                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, the user cannot be logged out',
                    'status'  => 500
                ], 500);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Parameter.',
                'status'  => 400
            ], 400);
        }
    }
	
	
	public function social_login(Request $request)
    {

            $user = User::where('app_id',$request->login_id)->where('app_type',$request->login_type)->first();

            if($user){
                $jwt_token=JWTAuth::fromUser($user);
                $messages = 'Login Successfull';
                $user['token']=$jwt_token;

                return response()->json([
                                'result' =>$user,
                                'message' => 'Login Success.',
                                'success' => true,
                                'status' => 200]);
            }
            else{

                $rules = array(
                    'login_type' => 'required|in:google,facebook',
                    'login_id' => 'required',
                    'name' => 'required',
                );

                $validator = \Validator::make($request->all(), $rules, []);

                if ($validator->fails()) {
                    $validation = $validator;
                    $status = false;
                    $code = 400;
                    $msgArr = $validator->messages()->toArray();
                    $messages = reset($msgArr)[0];
                    return response()->json([
                                'result' => JWTAuth::user(),
                                'message' =>$messages,
                                'success' => false,
                                'status' => $code,
                               ]);
                }
                 else {
                    $user = new User();
                    $user->name = $request->name;
                    $user->app_type = $request->login_type;
                    $user->app_id = $request->login_id;
                    $user->save();
                    $jwt_token=JWTAuth::fromUser($user);
                    $messages = 'Login Successfull';
                    $user_data=$user;
                    $user_data['token']=$jwt_token;
                    return response()->json([
                               'result' => $user_data,
                                'message' => $messages,
                                'success' => true,
                                'status' => 200
                               ]);

                }
            }
    }
    
	public function edit_profile(Request $request)
    {
        $user=JWTAuth::touser($request->token);
        if(isset($request->user_name))
        {
          $user->name=$request->user_name;
          $user->save();
          return response()->json([
            'result' => $user,
            'message' => 'Users Data Updated Successfully.',
            'success' => true,      
            'status' => 200,
          ]);
        }
        else
        {
          return response()->json([
              'success' => false,
              'message' => 'Invalid Parameter.',
              'status'  => 400
          ], 400);
        }
    }
	
	public function subscription(Request $request){
		$user=JWTAuth::touser($request->token);
		if(isset($user))
        {
          $user->plan_type=$request->plan_type;
          $user->expire_date=$request->expire_date;
          $user->plan_status=$request->plan_status;
          $user->is_free=$request->is_free;
          $user->save();
		  if($request->plan_status){
			 $message = 'Plan Activated Successfully.';
		  }else{
			  $message = 'Plan Deactivated Successfully.';
		  }
          return response()->json([
            'result' => $user,
            'message' => $message,
            'success' => true,      
            'status' => 200,
          ]);
        }
        else
        {
          return response()->json([
              'success' => false,
              'message' => 'Invalid Parameter.',
              'status'  => 400
          ], 400);
        }
	}
	
	public function log(Request $request){
		\DB::table('log')->insert(array('log'=>$request->log,'user_id'=>$request->user_id));
		return response()->json([
              'success' => true,
              'message' => 'Added',
              'status'  => 200
          ], 200);
	}
    
}
